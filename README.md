

## Getting Started

You should download this project. Next you should install maven.
````
sudo apt-get install maven

mvn compile

mvn package
````
### Run

Use this command to run the project. 
```
 mvn package

java -jar target/springBoot-1.0-SNAPSHOT.jar 

```

End with an example of getting some data out of the system or using it for a little demo

## Open Insomnia or postman

#### SignUp
Although some user already added to data base, I recommend to sign up.

Make post request.

Set url http://localhost:8080/signup
```
{
  "username": "Bob",
  "password": "123456"
}
```
#### SignIn

Now you should to sign in our project.

Make post request.

Set url http://localhost:8080/signin
```
{
  "username": "Bob",
  "password": "123456"
}
```

If you successful sign in you can see token, which you should copy.

```
{
  "token": "eyJhbGciOiJIUzUxMiJ9.eyJpc3MiOiJ0YW1wbGxlZXJyQGdtYWlsLmNvbSIsImlhdCI6MTU4NjcyMDY2MSwic3ViIjoiZml2ZSIsImp0aSI6IjA4ODQ4MzFiLTQyZjktNGY4MS1hZjg4LTNlNzc3YmU2OWM4ZiIsImV4cCI6MTU4NjcyMjQ2MSwiYXV0aG9yaXRpZXMiOlsiVVNFUiJdfQ.QCPpuEaZVngzTM8fYbvHgsoOipvQ1w2dn29Um40f96wNk1f_I2XuCvVXmRiNKFLQ69dm7BsuHGId_L3j9-z4pQ"
}
```

#### Authentication 
Now you should to set Authentication. 

 You can set it in header to write "Bearer ${token name}" or set in Auth "bearer token"
 and in the field "token" write your token.
 
 ## Let's start
 Now you can use this server!
 
 
#### Get Users
Set url http://localhost:8080/users
And make get request to check users
```
{
	
}
```


#### Get one user
Set url http://localhost:8080/users/{id}
And make get request to check user
```
{
	
}
```

#### Update one user
Set url http://localhost:8080/users/{id}
And make patch request to check update user
```
{
 
  "enabled": "false"
}
```
#### Who am i
Set url http://localhost:8080/whoami
And make get request to check who are you
```
{
 
}
```
#### Get one task
Set url http://localhost:8080/tasks/{id}
And make get request to check default task
```
{
	
}
```
#### Add task
Set url http://localhost:8080/tasks
And make post request
```
{
    "text"   : "Bob"
}
```
##### Get all task
Make get request to check all tasks

#### Patch task
Set url http://localhost:8080/tasks{id}
Make patch request
```
{
    "status" : "done"
}
```
#### Delete task
Set url http://localhost:8080/tasks{id}
Make delete request
```
{
  
}
```
## Deployment

Add additional notes about how to deploy this on a live system

## Built With
* [Maven](https://maven.apache.org/) - Dependency Management

## Authors

* **Vasilev Denis** - *Initial work*

