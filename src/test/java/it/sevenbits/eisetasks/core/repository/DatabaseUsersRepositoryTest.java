package it.sevenbits.eisetasks.core.repository;

import it.sevenbits.eisetasks.core.model.Task;
import it.sevenbits.eisetasks.core.model.User;
import it.sevenbits.eisetasks.core.repository.tasks.DatabaseTasksRepository;
import it.sevenbits.eisetasks.core.repository.users.DatabaseUsersRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.core.Authentication;

import java.util.Map;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

public class DatabaseUsersRepositoryTest {
    private JdbcOperations mockJdbcOperations;
    private DatabaseUsersRepository databaseUsersRepository;

    @Before
    public void setup() {
        mockJdbcOperations = mock(JdbcOperations.class);
        databaseUsersRepository = new DatabaseUsersRepository(mockJdbcOperations);
    }

    @Test
    public void testGetIDTasks() {
        User mockUser;
        UUID uuid=UUID.randomUUID();
        Map<String, Object> rawUser= mock(Map.class);
        when(mockJdbcOperations.queryForMap(anyString(), any(RowMapper.class), anyVararg())).thenReturn(rawUser);
        User expectedList = databaseUsersRepository.findByUserId(uuid.toString());
        mockUser=expectedList;
        verify(mockJdbcOperations, times(1)).queryForMap(
                eq(    "SELECT username, password, id FROM users u WHERE u.id = ?"),
                eq(uuid)

        );
        Assert.assertSame(expectedList, mockUser);
    }
}
