package it.sevenbits.eisetasks.core.repository;

import it.sevenbits.eisetasks.core.model.Task;
import it.sevenbits.eisetasks.core.model.User;
import it.sevenbits.eisetasks.core.repository.tasks.DatabaseTasksRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.core.Authentication;
import java.util.UUID;

import static org.mockito.Mockito.*;

public class DataBaseRepositoryTest {
    private JdbcOperations mockJdbcOperations;
    private DatabaseTasksRepository databaseTasksRepository;

    @Before
    public void setup() {
        mockJdbcOperations = mock(JdbcOperations.class);
        databaseTasksRepository = new DatabaseTasksRepository(mockJdbcOperations);
    }

    @Test
    public void testGetIDTasks() {
        Task mockTasks = mock(Task.class);
        User mockUser= mock(User.class);
        UUID uuid=UUID.randomUUID();
        Authentication authentication =mock(Authentication.class);
        when(mockJdbcOperations.queryForObject(anyString(), any(RowMapper.class), anyVararg())).thenReturn(mockTasks);
        when(mockUser.getId()).thenReturn(uuid);
        when( (User) authentication.getPrincipal()).thenReturn(mockUser);


        Task expectedList = databaseTasksRepository.getTask(uuid.toString(),authentication);
        verify(mockJdbcOperations, times(1)).queryForObject(
                eq("SELECT id, text, status,createdAt,updatedAt FROM task WHERE id  = ? AND owner =?"),
                any(RowMapper.class),
                eq(uuid),
                eq(uuid)
        );

        Assert.assertSame(expectedList, mockTasks);
    }
}
