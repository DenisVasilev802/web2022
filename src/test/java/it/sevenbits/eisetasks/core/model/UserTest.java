package it.sevenbits.eisetasks.core.model;

import org.junit.Test;

import java.util.List;
import java.util.UUID;

import static org.mockito.Mockito.*;
import static org.junit.Assert.assertEquals;

public class UserTest {
    @Test
    public void testBookModel() {
       String username="Bob";
       UUID id =UUID.fromString("1884831b-42f9-4f81-af88-3e777be69c8f");
       String password="password";
        List<String> authorities =mock(List.class);
        User user = new User(id.toString(),username,password,authorities);

        assertEquals(id, user.getId());
        assertEquals(username, user.getUsername());
        assertEquals(password, user.getPassword());
        assertEquals(authorities, user.getAuthorities());


    }
}
