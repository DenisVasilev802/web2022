package it.sevenbits.eisetasks.core.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TaskTest {
    @Test
    public void testBookModel() {
        String text = "Brian Goetz";

        String id = "11";
        String status="done";
        String createdAt="1.2.3000";
        String updatedAt="2.3.4000";
        String owner="2.3.4000";
        Task task = new Task(id,text,status,createdAt,updatedAt,owner);

        assertEquals(id, task.getId());
        assertEquals(text, task.getText());
        assertEquals(status, task.getStatus());
        assertEquals(createdAt, task.getCreatedAt());
        assertEquals(updatedAt, task.getUpdatedAt());

    }
}
