package it.sevenbits.eisetasks.web.controller;

import it.sevenbits.eisetasks.web.controllers.TasksController;
import it.sevenbits.eisetasks.core.model.Task;
import it.sevenbits.eisetasks.core.repository.tasks.DatabaseTasksRepository;
import it.sevenbits.eisetasks.core.model.SortTask;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;

import javax.swing.tree.RowMapper;
import java.util.List;
import java.util.UUID;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.*;

public class TaskControllerTest {
    private TasksController mockTasksController;

    private DatabaseTasksRepository databaseTasksRepository;

    @Before
    public void setup() {
        databaseTasksRepository = mock(DatabaseTasksRepository.class);
        mockTasksController = new TasksController(databaseTasksRepository);
    }

   @Test
    public void testGetAllTask() {
        List<Task> mockTasks = mock(List.class);
        SortTask sortTask = mock(SortTask.class);
        Authentication authentication = mock(Authentication.class);
        String mockTask = mockTasks.toString();
        when(databaseTasksRepository.getAllTasks(sortTask,authentication)).thenReturn(mockTask);

        ResponseEntity<String> answer = mockTasksController.list("asfasdasd",25,1);
        sortTask.checkDefault("asfasdasd", 25, 1,10,50);
        System.out.println(databaseTasksRepository.getAllTasks(sortTask,authentication));
        verify(databaseTasksRepository, times(1)).getAllTasks(sortTask,authentication);
        assertEquals(HttpStatus.OK, answer.getStatusCode());
    }



}