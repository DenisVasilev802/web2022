CREATE TABLE task
(
    id        uuid PRIMARY KEY,
    text      varchar   NOT NULL,
    createdAt timestamp with time zone NOT NULL,
    status    varchar DEFAULT 'inbox'
);
