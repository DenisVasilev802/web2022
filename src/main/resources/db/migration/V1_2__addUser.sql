create table users (
  id       uuid PRIMARY KEY,
  username VARCHAR(256),
  password VARCHAR(256),
  enabled boolean
);

create table authorities (
  userid    uuid REFERENCES users(id) ON DELETE CASCADE,
  authority VARCHAR(256),
  PRIMARY KEY (userid, authority)
);
