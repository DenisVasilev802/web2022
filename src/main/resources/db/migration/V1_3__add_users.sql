insert into users (id,username, password, enabled)
    values ('1884831b-42f9-4f81-af88-3e777be69c8f','four', '$2a$10$zNahRsBGIFD7HgY0bZrAB.z3JVjsKUgNCOMbyX6zzgvxqXcxz5mxy', true);

insert into authorities (userid,authority)
    values ('1884831b-42f9-4f81-af88-3e777be69c8f', 'ADMIN');

insert into authorities (userid, authority)
    values ('1884831b-42f9-4f81-af88-3e777be69c8f',  'USER');


insert into users (id,username, password, enabled)
    values ('0884831b-42f9-4f81-af88-3e777be69c8f', 'five','$2a$10$zNahRsBGIFD7HgY0bZrAB.z3JVjsKUgNCOMbyX6zzgvxqXcxz5mxy', true);

insert into authorities (userid, authority)
    values ('0884831b-42f9-4f81-af88-3e777be69c8f',  'USER');