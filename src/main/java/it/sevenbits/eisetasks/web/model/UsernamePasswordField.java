package it.sevenbits.eisetasks.web.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * Model to receive username and password.
 */
public class UsernamePasswordField {
    @NotNull
    @NotEmpty
    @NotBlank
    private final String login;
    @NotNull
    @NotEmpty
    @NotBlank
    private final String password;

    /**
     *
     * @param login login
     * @param password password
     */
    @JsonCreator
    public UsernamePasswordField(@JsonProperty("username") final String login, @JsonProperty("password")final String password) {
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

}
