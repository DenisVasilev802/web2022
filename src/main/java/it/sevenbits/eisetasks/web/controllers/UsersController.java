package it.sevenbits.eisetasks.web.controllers;

import com.fasterxml.jackson.annotation.JsonProperty;
import it.sevenbits.eisetasks.core.model.User;
import it.sevenbits.eisetasks.core.repository.users.IUsersRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

/**
 * Controller to list users.
 */
@Controller
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/users")
public class UsersController {

    private final IUsersRepository usersRepository;

    /**
     * @param usersRepository usersRepository
     */
    public UsersController(final IUsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }


    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<List<User>> getAllUsers() {
        return ResponseEntity.ok(usersRepository.findAll());
    }

    /**
     * @param id id
     * @return user Info
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<User> getUserInfo(final @PathVariable("id") String id) {
        return Optional
                .ofNullable(usersRepository.findByUserId(id))
                .map(user -> ResponseEntity.ok().body(user))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    /**
     * @param id         id
     * @param enableJson enable
     * @return user
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<User> updateEnable(final @PathVariable("id") String id, @RequestBody @Valid final Enabled enableJson) {

        Boolean enabled = enableJson.getEnabled();
        User user = usersRepository.findByUserId(id);
        if (user != null) {

            usersRepository.updateUser(user.getId().toString(), enabled);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    /**
     * Get json enable
     */
    private static class Enabled {
        @NotNull

        private final Boolean enabled;

        /**
         * @param enabled enable
         */
        Enabled(@JsonProperty("enabled") final Boolean enabled) {
            this.enabled = enabled;
        }

        Boolean getEnabled() {
            return enabled;
        }
    }
}

