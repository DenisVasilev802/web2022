package it.sevenbits.eisetasks.web.controllers;


import it.sevenbits.eisetasks.core.model.User;
import it.sevenbits.eisetasks.core.service.login.ILoginService;
import it.sevenbits.eisetasks.web.model.UsernamePasswordField;
import it.sevenbits.eisetasks.web.model.Token;
import it.sevenbits.eisetasks.web.security.IJwtTokenService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Performs login action.
 */
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/signin")
public class SignInController {

    private final ILoginService loginService;
    private final IJwtTokenService tokenService;

    /**
     * @param loginService loginService
     * @param tokenService tokenService
     */
    public SignInController(final ILoginService loginService, final IJwtTokenService tokenService) {
        this.loginService = loginService;
        this.tokenService = tokenService;
    }

    /**
     * @param login login
     * @return token
     */
    @PostMapping
    @ResponseBody
    public Token create(@RequestBody final UsernamePasswordField login) {
        User user = loginService.login(login);
        String token = tokenService.createToken(user);
        return new Token(token);
    }
}
