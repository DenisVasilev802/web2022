package it.sevenbits.eisetasks.web.controllers;

import it.sevenbits.eisetasks.core.repository.tasks.ITasksRepository;
import it.sevenbits.eisetasks.core.model.Task;
import it.sevenbits.eisetasks.core.model.SortTask;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Tasks Controller
 */
@CrossOrigin(origins = "http://localhost:3000")
@Controller
@RequestMapping("/tasks")
public class TasksController {

    @Value("${pagination.minPaginationPage}")
    private int min;
    @Value("${pagination.maxPaginationPage}")
    private int max;

    private final ITasksRepository tasksRepository;
    private SortTask sortTask;

    /**
     * @param tasksRepository TasksController
     */
    public TasksController(final ITasksRepository tasksRepository) {
        this.tasksRepository = tasksRepository;
        sortTask = new SortTask();
    }

    /**
     * method get
     *
     * @param order order task
     * @param size  size
     * @param page  page
     * @return get all task
     */
    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> list(@RequestParam(value = "order", required = false, defaultValue = "DESC") final String order,
                                       @RequestParam(value = "size", required = false, defaultValue = "25") final Integer size,
                                       @RequestParam(value = "page", required = false, defaultValue = "1") final Integer page) {
        sortTask.checkDefault(order, size, page, min, max);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String returnTasks = String.format("\"tasks\":%s,", tasksRepository.getAllTasks(sortTask, authentication));
        tasksRepository.countTask(sortTask);
        returnTasks += sortTask.toString();
        returnTasks = String.format("{%s}", returnTasks);

        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(returnTasks);


    }

    /**
     * @param newTask task
     * @return method post
     */
    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> create(@RequestBody final Task newTask) {


        if (newTask.getText() != null && !newTask.getText().equals("")) {
            if (newTask.getStatus() == null || newTask.getStatus().equals("done") || newTask.getStatus().equals("DONE") ||
                    !newTask.getStatus().equals("inbox")) {
                newTask.setDefaultStatus();
            }
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            Task createdTask = tasksRepository.create(newTask, authentication);
            return ResponseEntity.status(HttpStatus.CREATED).contentType(MediaType.APPLICATION_JSON_UTF8).body(createdTask.toString());
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

    }

    /**
     * @param id id
     * @return task
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<String> single(@PathVariable final String id) {
        if (id != null) {
            if (tasksRepository.isTask(id)) {
                Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
                return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON_UTF8).
                        body(tasksRepository.getTask(id, authentication).toString());
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * @param id task
     * @return method delete
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Task> delete(@PathVariable final String id) {
        if (tasksRepository.isTask(id)) {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            tasksRepository.deleteTask(id, authentication);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }


    }


    /**
     * @param newTask task
     * @param id      id
     * @return method patch
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Task> update(@PathVariable final String id, @RequestBody final Task newTask) {
        if (newTask.getStatus() != null || newTask.getText() != null) {
            if (newTask.getStatus() != null && (!newTask.getStatus().equals("done") && !newTask.getStatus().equals("DONE") &&
                    !newTask.getStatus().equals("inbox"))) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            } else if (newTask.getText() != null && (newTask.getText() == null || newTask.getText().equals(""))) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            } else {
                if (tasksRepository.isTask(id)) {
                    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
                    tasksRepository.updateTask(id, newTask.getStatus(), newTask.getText(), authentication);
                    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
                } else {
                    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
                }

            }
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }


    /**
     * @return quantity task
     */
    @RequestMapping(value = "/quantity", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<String> quantityTAsk() {

        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON_UTF8).
                body(sortTask.getQuantityTask());

    }


}

