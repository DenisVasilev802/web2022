package it.sevenbits.eisetasks.web.controllers;

import it.sevenbits.eisetasks.core.service.signup.ISignUpService;
import it.sevenbits.eisetasks.web.model.UsernamePasswordField;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;


/**
 * SignUpController
 */
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/signup")
public class SignUpController {

    private final ISignUpService signupService;

    /**
     * @param signupService signupService
     */
    public SignUpController(final ISignUpService signupService) {
        this.signupService = signupService;
    }

    /**
     * @param login login
     * @return token
     */
    @PostMapping
    @ResponseBody
    public ResponseEntity<String> create(@RequestBody @Valid final UsernamePasswordField login) {
            if (signupService.checkExist(login)) {
                signupService.singUp(login);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.CONFLICT);
            }



    }
}
