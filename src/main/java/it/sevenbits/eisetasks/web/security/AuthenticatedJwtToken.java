package it.sevenbits.eisetasks.web.security;

import it.sevenbits.eisetasks.core.model.User;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * Authorization which holds subject and roles/authorities from the JWT token.
 */
class AuthenticatedJwtToken extends AbstractAuthenticationToken {

    private final String subject;
    private final String id;

    /**
     *
     * @param subject sublect
     * @param id id
     * @param authorities authorities
     */
    AuthenticatedJwtToken(final String subject, final String id, final Collection<GrantedAuthority> authorities) {
        super(authorities);
        this.subject = subject;
        this.id = id;
        setAuthenticated(true);
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return new User(id, subject, null);
    }

}
