package it.sevenbits.eisetasks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * application
 */
@SpringBootApplication
public class Application {
    /**
     * main
     *
     * @param args arg
     */
    public static void main(final String[] args) {
        SpringApplication.run(Application.class, args);
    }
}