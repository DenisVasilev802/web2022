package it.sevenbits.eisetasks.core.repository.tasks;

import it.sevenbits.eisetasks.core.model.Task;
import it.sevenbits.eisetasks.core.model.SortTask;
import org.springframework.security.core.Authentication;

/**
 * TasksRepository
 */
public interface ITasksRepository {
    /**
     * @param sortTask sort task class
     * @param authentication authentication
     * @return get all tasks
     */
    String getAllTasks(SortTask sortTask, Authentication authentication);

    /**
     * @param id id
     * @param authentication authentication
     * @return get single task
     */
    Task getTask(String id, Authentication authentication);

    /**
     * @param item item
     * @param authentication authentication
     * @return new task
     */
    Task create(Task item, Authentication authentication);

    /**
     * @param id id
     * @return is task exist
     */
    boolean isTask(String id);

    /**
     * @param id     id
     * @param status change status
     * @param authentication authentication
     * @param text   change text
     */
    void updateTask(String id, String status, String text, Authentication authentication);

    /**
     * delete task
     * @param authentication authentication
     * @param id id
     */
    void deleteTask(String id, Authentication authentication);

    /**
     * @param sortTask quantity tasks
     */
    void countTask(SortTask sortTask);
}
