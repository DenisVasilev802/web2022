package it.sevenbits.eisetasks.core.repository.users;


import it.sevenbits.eisetasks.core.model.User;

import java.util.List;

/**
 * interface for user repository
 */
public interface IUsersRepository {
    /**
     * @param username username
     * @return user
     */
    User findByUserName(String username);

    /**
     * @param id id
     * @return user
     */
    User findByUserId(String id);

    /**
     * @return users
     */
    List<User> findAll();

    /**
     * @param username username
     * @param password password
     * @param roles    roles
     */
    void addUser(String username, String password, List<String> roles);

    /**
     * @param id      id
     * @param enabled enabled
     */
    void updateUser(String id, boolean enabled);
}
