package it.sevenbits.eisetasks.core.repository.tasks;


import it.sevenbits.eisetasks.core.model.Task;
import it.sevenbits.eisetasks.core.model.SortTask;
import org.springframework.security.core.Authentication;

import java.util.Date;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Simple Tasks Repository
 */
public class SimpleTasksRepository implements ITasksRepository {
    private ConcurrentHashMap<String, Task> tasks = new ConcurrentHashMap<>();

    /**
     * Simple Tasks Repository
     */
    public SimpleTasksRepository() {
    }

    /**
     * @return tasks
     */
    @Override
    public String getAllTasks(final SortTask sortTask, final Authentication authentication) {
        return tasks.values().toString();
    }

    @Override
    public Task getTask(final String id, final Authentication authentication) {
        return tasks.get(id);
    }


    /**
     * @param task id name
     * @return crate task
     */
    @Override
    public Task create(final Task task, final Authentication authentication) {
        Task newTask = new Task(getNextID(), task.getText(), task.getStatus(), createDate(), createDate(), null);
        tasks.put(newTask.getId(), newTask);
        return newTask;
    }

    /**
     * @return random uuid
     */
    private String getNextID() {
        return UUID.randomUUID().toString();
    }

    /**
     * Is Exist task
     *
     * @param id id
     * @return exist or not
     */
    @Override
    public boolean isTask(final String id) {
        return tasks.containsKey(id);
    }

    @Override
    public void updateTask(final String id, final String status, final String text, final Authentication authentication) {
        tasks.get(id).setStatus(status);
        tasks.get(id).setUpdatedAt(createDate());
    }

    @Override
    public void deleteTask(final String id, final Authentication authentication) {
        tasks.remove(id);
    }

    @Override
    public void countTask(final SortTask sortTask) {
    }

    private String createDate() {
        Date date = new Date();
        return date.toString();
    }
}
