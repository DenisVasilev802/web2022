package it.sevenbits.eisetasks.core.repository.tasks;

import it.sevenbits.eisetasks.core.model.Task;
import it.sevenbits.eisetasks.core.model.SortTask;
import it.sevenbits.eisetasks.core.model.User;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.security.core.Authentication;

import java.time.OffsetDateTime;
import java.util.UUID;

/**
 * Data base task repository
 */
public class DatabaseTasksRepository implements ITasksRepository {
    private JdbcOperations jdbcOperations;
    private static final int FIRST = 1;
    private static final int SECOND = 2;
    private static final int THIRD = 3;
    private static final int FORTH = 4;
    private static final int FIFTH = 5;

    /**
     * @param jdbcOperations jdbcOperations
     */
    public DatabaseTasksRepository(final JdbcOperations jdbcOperations) {
        this.jdbcOperations = jdbcOperations;
    }

    /**
     * @return all task
     */
    @Override
    public String getAllTasks(final SortTask sortTask, final Authentication authentication) {
        User principal = (User) authentication.getPrincipal();
        UUID userId = UUID.fromString(principal.getId().toString());
        return jdbcOperations.query(
                String.format("SELECT id, text, status,createdat,updatedAt FROM task WHERE owner  = ?" +
                                " ORDER BY createdat %s LIMIT %d OFFSET %d",
                        sortTask.getOrder(), sortTask.getSize(), (sortTask.getPage() - 1) * sortTask.getSize()),
                (resultSet, i) -> {
                    UUID id = UUID.fromString(resultSet.getString(FIRST));
                    String text = resultSet.getString(SECOND);
                    String status = resultSet.getString(THIRD);
                    String createdAt = resultSet.getString(FORTH);
                    String updatedAt = resultSet.getString(FIFTH);
                    return new Task(String.valueOf(id), text, status, createdAt, updatedAt, null);
                }, userId).toString();

    }


    @Override
    public Task getTask(final String id, final Authentication authentication) {
        User principal = (User) authentication.getPrincipal();
        UUID userId = UUID.fromString(principal.getId().toString());
        return jdbcOperations.queryForObject(
                "SELECT id, text, status,createdAt,updatedAt FROM task WHERE id  = ? AND owner =?",
                (resultSet, i) -> {
                    UUID rowId = UUID.fromString(resultSet.getString(FIRST));
                    String text = resultSet.getString(SECOND);
                    String status = resultSet.getString(THIRD);
                    String createdAt = resultSet.getString(FORTH);
                    String updatedAt = resultSet.getString(FIFTH);
                    return new Task(String.valueOf(rowId), text, status, createdAt, updatedAt, null);
                },
                UUID.fromString(id), userId);
    }

    @Override
    public Task create(final Task newTask, final Authentication authentication) {
        User principal = (User) authentication.getPrincipal();
        UUID userId = UUID.fromString(principal.getId().toString());
        UUID id = UUID.randomUUID();
        String text = newTask.getText();
        String status = newTask.getStatus();
        OffsetDateTime createdAt = OffsetDateTime.now();
        int rows = jdbcOperations.update(
                "INSERT INTO task (id, text, status,createdAt,updatedAt,owner) VALUES (?, ?, ?, ?, ?, ?)",
                id, text, status, createdAt, createdAt, userId
        );
        return new Task(String.valueOf(id), text, status, createDate(), createDate(), null);
    }

    @Override
    public boolean isTask(final String id) {
        String sql = "SELECT count(*) FROM task WHERE id = ?";
        int count = jdbcOperations.queryForObject(sql, new Object[]{UUID.fromString(id)}, Integer.class);
        return count > 0;
    }

    @Override
    public void countTask(final SortTask sortTask) {

        int countTask = jdbcOperations.queryForObject("SELECT count(*) FROM task",
                Integer.class);
        int done = jdbcOperations.queryForObject("SELECT count(status)as count FROM task WHERE status ='done'",
                Integer.class);
        sortTask.setDoneTask(done);
        sortTask.setTotalTask(countTask);
        sortTask.setTodoTask(countTask - done);
    }

    @Override
    public void updateTask(final String id, final String status, final String text, final Authentication authentication) {
        User principal = (User) authentication.getPrincipal();
        UUID userId = UUID.fromString(principal.getId().toString());
        OffsetDateTime updatedAt = OffsetDateTime.now();
        String updateString;
        if (status == null) {
            updateString = "UPDATE task SET text = ?,updatedAt = ? WHERE id = ? AND owner = ?";
            int rows = jdbcOperations.update(
                    updateString,
                    text, updatedAt, UUID.fromString(id), userId
            );
        } else if (text == null) {
            updateString = "UPDATE task SET status = ?,updatedAt = ? WHERE id = ?AND owner = ?";
            int rows = jdbcOperations.update(
                    updateString,
                    status, updatedAt, UUID.fromString(id), userId
            );
        } else {
            updateString = "UPDATE task SET status = ?,updatedAt = ?,text = ? WHERE id = ?";
            int rows = jdbcOperations.update(
                    updateString,
                    status, updatedAt, text, UUID.fromString(id), userId
            );
        }
    }

    @Override
    public void deleteTask(final String id, final Authentication authentication) {
        User principal = (User) authentication.getPrincipal();
        UUID userId = UUID.fromString(principal.getId().toString());
        jdbcOperations.update("DELETE FROM task WHERE id = ? AND owner = ?", UUID.fromString(id), userId);


    }

    private String createDate() {
        OffsetDateTime date = OffsetDateTime.now();
        return date.toString();
    }
}
