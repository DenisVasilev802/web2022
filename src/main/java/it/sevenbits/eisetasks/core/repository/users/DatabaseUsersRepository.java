package it.sevenbits.eisetasks.core.repository.users;


import it.sevenbits.eisetasks.core.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Repository to list all users.
 */
public class DatabaseUsersRepository implements IUsersRepository {
    @Autowired
    private PasswordEncoder passwordEncoder;
    private final JdbcOperations jdbcOperations;
    private static final String AUTHORITY = "authority";
    private static final String USERNAME = "username";
    private static final String PASSWORD = "password";
    private static final String ID = "id";
    private static final String USERID = "userid";

    /**
     * @param jdbcOperations jdbcOperations
     */
    public DatabaseUsersRepository(final JdbcOperations jdbcOperations) {
        this.jdbcOperations = jdbcOperations;
    }

    /**
     * @param username username
     * @return Map
     */
    public User findByUserName(final String username) {
        Map<String, Object> rawUser;

        try {
            rawUser = jdbcOperations.queryForMap(
                    "SELECT username, password, id FROM users u" +
                            " WHERE  u.username = ?",
                    username
            );
        } catch (IncorrectResultSizeDataAccessException e) {
            return null;
        }

        List<String> authorities = getAuthorities(String.valueOf(rawUser.get(ID)));
        String password = String.valueOf(rawUser.get(PASSWORD));
        String id = String.valueOf(rawUser.get(ID));
        return new User(id, username, password, authorities);
    }

    /**
     * find By User Id
     *
     * @param id id
     * @return user
     */
    public User findByUserId(final String id) {
        Map<String, Object> rawUser;

        try {
            rawUser = jdbcOperations.queryForMap(
                    "SELECT username, password, id FROM users u" +
                            " WHERE u.id = ?",
                    UUID.fromString(String.valueOf(id))
            );
        } catch (IncorrectResultSizeDataAccessException e) {
            return null;
        }

        List<String> authorities = getAuthorities(id);


        String password = String.valueOf(rawUser.get(PASSWORD));
        String username = String.valueOf(rawUser.get(USERNAME));
        return new User(id, username, password, authorities);
    }

    /**
     * find all users
     *
     * @return users
     */
    public List<User> findAll() {
        HashMap<String, User> users = new HashMap<>();

        for (Map<String, Object> row : jdbcOperations.queryForList(
                "SELECT * FROM authorities a,users u" +
                        " WHERE" +
                        " u.id = a.userid AND u.enabled = true")) {

            String id = String.valueOf(row.get(USERID));
            String newRole = String.valueOf(row.get(AUTHORITY));
            String username = String.valueOf(row.get(USERNAME));
            User user = users.computeIfAbsent(id, uuid -> new User(uuid, username, new ArrayList<>()));
            List<String> roles = user.getAuthorities();
            roles.add(newRole);

        }

        return new ArrayList<>(users.values());
    }

    @Override
    public void addUser(final String username, final String password, final List<String> roles) {
        UUID id = UUID.randomUUID();
        int rows = jdbcOperations.update(
                "INSERT INTO users (id,username,password,enabled) VALUES (?,?,?,?)",
                id, username, passwordEncoder.encode(password), true
        );
        for (String role : roles) {
            jdbcOperations.update(
                    "INSERT INTO authorities (userid,authority) VALUES (?,?)",
                    id, role
            );
        }

    }

    @Override
    public void updateUser(final String id, final boolean enabled) {
        String updateString;
        updateString = "UPDATE users SET enabled = ? WHERE id = ?";
        int rows = jdbcOperations.update(
                updateString,
                enabled, UUID.fromString(id)
        );

    }

    private List<String> getAuthorities(final String id) {
        List<String> authorities = new ArrayList<>();
        jdbcOperations.query(
                "SELECT userid, authority FROM authorities" +
                        " WHERE userid = ?",
                resultSet -> {
                    String authority = resultSet.getString(AUTHORITY);
                    authorities.add(authority);
                },
                UUID.fromString(String.valueOf(id))
        );
        return authorities;
    }

}
