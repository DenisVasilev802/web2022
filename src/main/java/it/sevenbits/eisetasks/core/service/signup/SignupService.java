package it.sevenbits.eisetasks.core.service.signup;

import it.sevenbits.eisetasks.core.repository.users.IUsersRepository;
import it.sevenbits.eisetasks.web.model.UsernamePasswordField;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

/**
 * SignupService
 */
@Service
public class SignupService implements ISignUpService {

    private final IUsersRepository usersRepository;
    private final PasswordEncoder passwordEncoder;
    private static final List<String> DEFAULT_AUTHORITIES = Collections.singletonList("USER");

    /**
     * @param users           users
     * @param passwordEncoder passwordEncoder
     */

    public SignupService(final IUsersRepository users, final PasswordEncoder passwordEncoder) {
        this.usersRepository = users;
        this.passwordEncoder = passwordEncoder;
    }

    /**
     *
     * @param login login
     * @return check
     */
    @Override
    public boolean checkExist(final UsernamePasswordField login) {
        return usersRepository.findByUserName(login.getLogin()) == null;
    }

    /**
     *
     * @param login login
     */
    @Override
    public void singUp(final UsernamePasswordField login) {
        usersRepository.addUser(login.getLogin(), login.getPassword(), DEFAULT_AUTHORITIES);
    }

}
