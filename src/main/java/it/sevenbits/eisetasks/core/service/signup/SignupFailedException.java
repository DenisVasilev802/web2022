package it.sevenbits.eisetasks.core.service.signup;

import org.springframework.security.core.AuthenticationException;

/**
 * SignupFailedException
 */
public class SignupFailedException extends AuthenticationException {
    /**
     * @param message message
     * @param cause   cause
     */
    public SignupFailedException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * @param message message
     */
    public SignupFailedException(final String message) {
        super(message);
    }

}
