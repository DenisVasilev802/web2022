package it.sevenbits.eisetasks.core.service.login;

import org.springframework.security.core.AuthenticationException;

/**
 * LoginFailedException
 */
public class LoginFailedException extends AuthenticationException {
    /**
     * @param message message
     * @param cause   cause
     */
    public LoginFailedException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * @param message message
     */
    public LoginFailedException(final String message) {
        super(message);
    }

}
