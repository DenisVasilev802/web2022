package it.sevenbits.eisetasks.core.service.signup;

import it.sevenbits.eisetasks.web.model.UsernamePasswordField;

/**
 * interface for sign in service
 */
public interface ISignUpService {
    /**
     * check user existence
     *
     * @param login login
     * @return check exist
     */
    boolean checkExist(UsernamePasswordField login);

    /**
     * sign up
     * @param login sign up
     */
    void singUp(UsernamePasswordField login);
}
