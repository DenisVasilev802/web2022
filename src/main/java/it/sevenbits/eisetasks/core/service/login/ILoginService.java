package it.sevenbits.eisetasks.core.service.login;

import it.sevenbits.eisetasks.core.model.User;
import it.sevenbits.eisetasks.web.model.UsernamePasswordField;

/**
 * interface for login service
 */
public interface ILoginService {
    /**
     * login in system
     *
     * @param login login
     * @return log in
     */
    User login(UsernamePasswordField login);
}
