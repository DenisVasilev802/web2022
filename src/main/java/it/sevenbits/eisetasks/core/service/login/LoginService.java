package it.sevenbits.eisetasks.core.service.login;

import it.sevenbits.eisetasks.core.model.User;
import it.sevenbits.eisetasks.core.repository.users.IUsersRepository;
import it.sevenbits.eisetasks.web.model.UsernamePasswordField;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * LoginService
 */
@Service
public class LoginService implements ILoginService {

    private final IUsersRepository users;
    private final PasswordEncoder passwordEncoder;

    /**
     * @param users           users
     * @param passwordEncoder passwordEncoder
     */
    public LoginService(final IUsersRepository users, final PasswordEncoder passwordEncoder) {
        this.users = users;
        this.passwordEncoder = passwordEncoder;
    }

    /**
     * @param login login
     * @return user
     */
    @Override
    public User login(final UsernamePasswordField login) throws LoginFailedException {
        User user = users.findByUserName(login.getLogin());

        if (user == null) {
            throw new LoginFailedException("User '" + login.getLogin() + "' not found");
        }

        if (!passwordEncoder.matches(login.getPassword(), user.getPassword())) {
            throw new LoginFailedException("Wrong password");
        }
        return new User(String.valueOf(user.getId()), user.getUsername(), user.getAuthorities());
    }

}
