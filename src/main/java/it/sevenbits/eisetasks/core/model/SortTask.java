package it.sevenbits.eisetasks.core.model;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.stereotype.Component;

/**
 * set param to sort tasks
 */
@Component
@SpringBootConfiguration
public class SortTask {
    private static final String DEFAULT_ORDER = "DESC";
    private static final int DEFAULT_SIZE = 25;
    private static final int DEFAULT_PAGE = 1;
    private int minSize;
    private int maxSize;
    private int size;
    private int page;
    private String order;
    private int doneTask = 0;
    private int totalTask = 0;
    private int todoTask = 0;
    private String next = null;
    private String prev = null;
    private String first = null;
    private String last = null;
    private final String status = "done";


    public int getTotalTask() {
        return totalTask;
    }

    /**
     * @param totalTask set total task
     */
    public void setTotalTask(final int totalTask) {
        setPages(totalTask);
        this.totalTask = totalTask;
    }

    public int getTodoTask() {
        return todoTask;
    }

    public void setTodoTask(final int todoTask) {
        this.todoTask = todoTask;
    }

    public int getSize() {
        return size;
    }

    public void setSize(final int size) {
        this.size = size;
    }

    public int getPage() {
        return page;
    }

    public void setPage(final int page) {
        this.page = page;
    }

    public String getOrder() {
        return order;
    }

    public int getDoneTask() {
        return doneTask;
    }

    public void setDoneTask(final int doneTask) {
        this.doneTask = doneTask;
    }

    public void setOrder(final String order) {
        this.order = order;
    }

    /**
     * set default param
     *
     * @param order1 order
     * @param size1  size
     * @param page1  page
     * @param min    min
     * @param max    max
     */
    public void checkDefault(final String order1, final int size1, final int page1, final int min, final int max) {
        this.maxSize = max;
        this.minSize = min;
        if ((order1.equals("asc") || order1.equals("ASC"))) {

            this.order = order1;
        } else {
            this.order = DEFAULT_ORDER;
        }
        if (size1 < minSize || size1 > maxSize) {
            this.size = DEFAULT_SIZE;
        } else {
            this.size = size1;
        }
        if (page1 < 0) {
            this.page = DEFAULT_PAGE;
        } else {
            this.page = page1;
        }


    }

    @Override
    public String toString() {
        return "\"_meta\":{" +
                "\"total\" :\"" + totalTask + "\"," +
                "\"page\" :\"" + page + "\"," +
                "\"size\" :\"" + size + "\"," +
                "\"order\" :\"" + order + "\"," +
                "\"status\" :\"" + status + "\"," +
                "\"next\" :\"" + next + "\"," +
                "\"prev\" :\"" + prev + "\"," +
                "\"first\" :\"" + first + "\"," +
                "\"last\" :\"" + last + "\"" +

                '}';
    }

    private void setPages(final int totalTasks) {

        int maxPage = totalTasks / size;
        if (maxPage == 0) {
            maxPage = 0;
        }
        int nextPage = page;
        if (page + 1 < maxPage) {
            nextPage = page + 1;
        }
        int prevPage = page;
        if (page - 1 > 0) {
            prevPage = page - 1;
        }
        first = String.format("/task?status=done&order=%s&page=%d&size=%d", order, 1, size);
        prev = String.format("/task?status=done&order=%s&page=%d&size=%d", order, prevPage, size);
        next = String.format("/task?status=done&order=%s&page=%d&size=%d", order, nextPage, size);
        last = String.format("/task?status=done&order=%s&page=%d&size=%d", order, maxPage, size);

    }

    public String getQuantityTask() {
        return "{" +
                "\"done\" :\"" + doneTask + "\"," +
                "\"todo\" :\"" + todoTask + "\"" +
                '}';
    }
}
