package it.sevenbits.eisetasks.core.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * task
 */
public class Task {
    private static final String DEFAULT_STATUS = "inbox";
    private String id;
    private String text;
    private String status;
    private String createdAt;
    private String updatedAt;
    private String owner;

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(final String createdAt) {
        this.createdAt = createdAt;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public void setText(final String text) {
        this.text = text;
    }

    public void setStatus(final String status) {
        this.status = status;
    }

    /**
     * @param id        example : 01f2-d123
     * @param text      example :  Make bread
     * @param status    example : inbox
     * @param createdAt example : 01.02.2000
     * @param updatedAt example : 01.02.2000
     * @param owner     example : 01f2-d123
     */
    @JsonCreator
    public Task(@JsonProperty("id") final String id, @JsonProperty("text") final String text,
                @JsonProperty("status") final String status, final String createdAt, final String updatedAt, final String owner) {
        this.id = id;
        this.text = text;
        this.status = status;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.owner = owner;
    }

    public String getOwner() {
        return owner;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(final String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return "{" +
                "\"id\":\"" + id + '\"' +
                ", \"text\":\"" + text + '\"' +
                ", \"status\":\"" + status + '\"' +
                ", \"createdAt\":\"" + createdAt + '\"' +
                ", \"updatedAt\":\"" + updatedAt + '\"' +
                '}';
    }

    public String getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public String getStatus() {
        return status;
    }

    /**
     * set default status
     */
    public void setDefaultStatus() {
        status = DEFAULT_STATUS;
    }

}
