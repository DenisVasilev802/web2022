package it.sevenbits.eisetasks.core.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * user
 */
public class User {

    @JsonProperty("username")
    private final String username;

    @JsonProperty("authorities")
    private final List<String> authorities;

    @JsonIgnore
    private final String password;
    @JsonProperty("id")
    private final UUID id;

    /**
     *
     * @param id id
     * @param username username
     * @param password password
     * @param authorities authorities
     */
    public User(final String id, final String username, final String password, final List<String> authorities) {
        this.username = username;
        this.password = password;
        this.authorities = authorities;
        this.id = UUID.fromString(id);
    }

    /**
     *
     * @param id id
     * @param username username
     * @param authorities authorities
     */
    @JsonCreator
    public User(final String id, final String username, final List<String> authorities) {
        this.username = username;
        this.password = null;
        this.authorities = authorities;
        this.id = UUID.fromString(id);
    }

    public UUID getId() {
        return id;
    }

    /**
     *
     * @param authentication authentication
     */
    public User(final Authentication authentication) {

        User principal = (User) authentication.getPrincipal();
        id = UUID.fromString(principal.getId().toString());
        if (principal instanceof UserDetails) {
            username = ((UserDetails) principal).getUsername();
        } else {
            username = principal.getUsername();
        }

        password = null;

        authorities = new ArrayList<>();
        for (GrantedAuthority authority : authentication.getAuthorities()) {
            authorities.add(authority.getAuthority());
        }
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public List<String> getAuthorities() {
        return authorities;
    }
}
