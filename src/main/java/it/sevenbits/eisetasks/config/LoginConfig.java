package it.sevenbits.eisetasks.config;


import it.sevenbits.eisetasks.core.repository.users.IUsersRepository;
import it.sevenbits.eisetasks.core.service.login.ILoginService;
import it.sevenbits.eisetasks.core.service.login.LoginService;
import it.sevenbits.eisetasks.web.controllers.SignInController;
import it.sevenbits.eisetasks.web.security.IJwtTokenService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * LoginConfig
 */
@Configuration
public class LoginConfig {
    /**
     * @param loginService    loginService
     * @param jwtTokenService jwtTokenService
     * @return object
     */
    @Bean
    public Object loginController(final ILoginService loginService, final IJwtTokenService jwtTokenService) {
        return new SignInController(loginService, jwtTokenService);
    }

    /**
     *
     * @param users user
     * @param passwordEncoder passwordEncoder
     * @return login service
     */
    @Bean
    public ILoginService iLoginService(final IUsersRepository users, final PasswordEncoder passwordEncoder) {
        return new LoginService(users, passwordEncoder);
    }
}
