package it.sevenbits.eisetasks.config;

import it.sevenbits.eisetasks.core.repository.tasks.DatabaseTasksRepository;
import it.sevenbits.eisetasks.core.repository.tasks.ITasksRepository;
import it.sevenbits.eisetasks.core.repository.users.DatabaseUsersRepository;
import it.sevenbits.eisetasks.core.repository.users.IUsersRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * RepositoryConfig
 */
@Configuration
public class RepositoryConfig {
    /**
     * The method creates instance of books repository
     * @param jdbcTemplate instance of jdbcTemplate
     * @return instance of the books repository
     */
    @Bean
    public ITasksRepository booksRepository(final @Qualifier("JdbcTemplate") JdbcTemplate jdbcTemplate) {
        return new DatabaseTasksRepository(jdbcTemplate);
    }

    /**
     * The method creates instance of users repository
     * @param jdbcTemplate instance of jdbcTemplate
     * @return instance of the books repository
     */
    @Bean
    public IUsersRepository usersRepository(final @Qualifier("JdbcTemplate") JdbcTemplate jdbcTemplate) {
        return new DatabaseUsersRepository(jdbcTemplate);
    }
}
