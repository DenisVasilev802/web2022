package it.sevenbits.eisetasks.config;

import it.sevenbits.eisetasks.core.repository.users.IUsersRepository;
import it.sevenbits.eisetasks.core.service.signup.ISignUpService;
import it.sevenbits.eisetasks.core.service.signup.SignupService;
import it.sevenbits.eisetasks.web.controllers.SignUpController;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * SignUpConfig
 */
@Configuration
public class SignUpConfig {
    /**
     * @param signupService signupService
     * @return object
     */
    @Bean
    public Object signUpController(final ISignUpService signupService) {
        return new SignUpController(signupService);
    }

    /**
     * SignupService
     * @param users users
     * @param passwordEncoder passwordEncoder
     * @return  SignupService
     */
    @Bean
    public ISignUpService iSignUpService(final IUsersRepository users, final PasswordEncoder passwordEncoder) {
        return new SignupService(users, passwordEncoder);
    }
}
